#include<iostream>
using namespace std;
class A
{
public:
virtual void print()
{cout<<"This is a base class print"<<endl;}
void show()
{cout<<"This is a base class show"<<endl;}
};
class B:public A
{
public:
void print()
{cout<<"This is a child class print"<<endl;}
void show()
{cout<<"This is a child class show"<<endl;}
};

int main()
{A* a1;
 B b1;
 a1=&b1;
 a1->print();
 a1->show();
return 0;
}
