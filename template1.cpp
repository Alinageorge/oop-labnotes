#include<iostream>
using namespace std;
template<typename T>T big(T x,T y)
{return(x>y)?x:y;
}
int main()
{cout<<big<int>(10,4)<<endl;
cout<<big<double>(20.5,70.3)<<endl;
cout<<big<char>('b','h')<<endl;
return 0;
}
